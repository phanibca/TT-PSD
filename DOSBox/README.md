PSD-case-studies
=================================================
Setting up Cloudbees
-------------------------------------------------
If you plan on using cloudbees, it is much easier if you use the cloudbees repository as a starting point.
Here is how:

1) mkdir MyExample

2) cd MyExample

3) git clone https://github.com/scrumorg/PSD-case-studies.git

4) go to cloudbees and make a new repository (in this example dos123)

5) in cloudbees copy the link for the repository (for example https://git.cloudbees.com/psddeveloper/dos123.git) to your clipboard

6) copy the current code from your computer to the cloudbees repository using this:

git push https://git.cloudbees.com/psddeveloper/dos123.git 

the repository name is in your clipboard and is only an example

7) now copy an existing build in cloudbees 

-select "New Job" from the left menu

-select "Copy existing job" and select the example build as existing job ("_example_teamX_svn" or "_example_teamX_git").

8) configure the build and make sure the root pom points to: java/DOSBox/pom.xml

9) you should be able to run a "build now"



Setting up Eclipse
-------------------------------------------------

After having cloned the repository, open Eclipse, select File » Import and then Import Existing Maven Project. Locate the directory where you just cloned this repo to, select it and hit Finish button.
